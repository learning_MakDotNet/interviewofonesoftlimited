﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InterviewOfOneSoftLimited.Models
{
    public class Language
    {
        [Key]
        public int LanguageId { get; set; }

        //[Required(ErrorMessage = "Select one or more language!")]
        public string LanguageName { get; set; }
        [NotMapped]
        public bool IsSeleted { get; set; }

        public virtual ICollection<LanguageDetail> LanguageDetails { get; set; }
    }
}