﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InterviewOfOneSoftLimited.Models
{
    public class PersonInfo
    {
        [Key]
        public int PersonId { get; set; }

        [Required(ErrorMessage = "Must fill up name!")]
        [MaxLength(32)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Select a country!")]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        [Required(ErrorMessage = "Select a city!")]
        [ForeignKey("City")]
        public int CityId { get; set; }
        
        [Required(ErrorMessage = "Must fill up date of birth!")]
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Resume not show and file must be pdf or doc!")]
        public string Resume { get; set; }


        //Navigation 
        public virtual Country Country { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<LanguageDetail> LanguageDetails { get; set; }
    }
}