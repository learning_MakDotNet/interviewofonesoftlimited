﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InterviewOfOneSoftLimited.Models
{
    public class LanguageDetail
    {
        [Key]
        public int LanguageDetailId { get; set; }

        [ForeignKey("Language")]
        public int LanguageId { get; set; }

        [ForeignKey("PersonInfo")]
        public int PersonId { get; set; }

        //Navigation
        public virtual Language Language { get; set; }
        public virtual PersonInfo PersonInfo { get; set; }
    }
}