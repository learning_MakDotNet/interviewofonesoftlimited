﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InterviewOfOneSoftLimited.Models
{
    public class InterviewDbContext : DbContext
    {
        public InterviewDbContext() : base("InterviewDbContext")
        {

        }
        
        public DbSet<PersonInfo> PersonInfos { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<LanguageDetail> LanguageDetails { get; set; }
    }
}