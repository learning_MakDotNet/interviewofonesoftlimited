﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InterviewOfOneSoftLimited.Models.ViewModel
{
    public class PersonInfoViewModel
    {
        [Key]
        public int PersonId { get; set; }

        [Required(ErrorMessage = "Must fill up name!")]
        [MaxLength(32)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Select a country!")]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        [Required(ErrorMessage = "Select a city!")]
        [ForeignKey("City")]
        public int CityId { get; set; }
        
        [Required(ErrorMessage = "Must fill up date of birth!")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Resume not show and file must be pdf or doc!")]
        public HttpPostedFileBase Resume { get; set; }

        [Required(ErrorMessage = "Select one or more language!")]
        public List<Language> Languages { get; set; }
        
        public PersonInfoViewModel()
        {
            this.Languages = new List<Language>();
        }
    }


}