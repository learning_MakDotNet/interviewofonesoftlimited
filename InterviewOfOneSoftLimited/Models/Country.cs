﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InterviewOfOneSoftLimited.Models
{
    public class Country
    {
        [Key]
        public int CountryId { get; set; }

       // [Required]
        public string CountryName { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public virtual ICollection<PersonInfo> PersonInfos { get; set; }
    }
}