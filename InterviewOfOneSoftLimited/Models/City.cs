﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace InterviewOfOneSoftLimited.Models
{
    public class City
    {
        [Key]
        public int CityId { get; set; }

        // [Required]
        public string CityName { get; set; }

        //[Required(ErrorMessage = "Select a country!")]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<PersonInfo> PersonInfos { get; set; }
    }
}