﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InterviewOfOneSoftLimited.Models;
using InterviewOfOneSoftLimited.Models.ViewModel;

namespace InterviewOfOneSoftLimited.Controllers
{
    public class PersonInfoController : Controller
    {
        private InterviewDbContext db = new InterviewDbContext();
        
        public ActionResult Index()
        {
            var personInfos = db.PersonInfos.Include(p => p.City).Include(p => p.Country).Include(p =>p.LanguageDetails).ToList();
            return View(personInfos);
        }

        public ActionResult Details(int? id)
        {
            PersonInfo personInfo = db.PersonInfos.Find(id);
            return View(personInfo);
        }
        public JsonResult GetEmps(string CountryId)
        {
            int id = Convert.ToInt32(CountryId);
            var data = db.Cities.Where(e => e.CountryId ==id ).Select(e => new { e.CityId, e.CityName }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Create()
        {
            ViewBag.Country = db.Countries.ToList();
            ViewBag.CityId = new SelectList(db.Cities.ToList(), "CityId", "CityName");
            ViewBag.CountryId = new SelectList(db.Countries.ToList(), "CountryId", "CountryName");
            List<Language> lang = new List<Language>();
             lang = db.Languages.ToList();
            ViewBag.LanguageDetail = lang;
            PersonInfoViewModel model = new PersonInfoViewModel();
            foreach (var l in lang)
            {
                model.Languages.Add(new Language { LanguageId=l.LanguageId,LanguageName=l.LanguageName,IsSeleted=false});
            }
            return View(model);
        }
        
        [HttpPost]
        public ActionResult Create(PersonInfoViewModel personInfo,string hobbies)
        {
            List<Language> lang = new List<Language>();
            lang = db.Languages.ToList();
            ViewBag.LanguageDetail = lang;

            PersonInfo person = new PersonInfo();
            LanguageDetail languageDetail = new LanguageDetail();
            if (ModelState.IsValid)
            {
                string fileName = null;
                if (personInfo.Resume.ContentLength > 0)
                {
                    fileName = Path.GetFileNameWithoutExtension(personInfo.Resume.FileName);
                    string extension = Path.GetExtension(personInfo.Resume.FileName);
                    fileName = fileName +"_"+ DateTime.Now.ToString("yymmssfff") + extension;
                    string filePath = Path.Combine(Server.MapPath("~/Images"), fileName);
                    personInfo.Resume.SaveAs(filePath);
                }
                person.Name = personInfo.Name;
                person.CountryId = personInfo.CountryId;
                person.CityId = personInfo.CityId;
                person.DateOfBirth = personInfo.DateOfBirth;
                person.Resume = fileName;
                db.PersonInfos.Add(person);
                db.SaveChanges();

                hobbies= hobbies + ",";
                string s = "";
                foreach (var i in hobbies)
                {
                    if (i == ',')
                    {
                        int id = Convert.ToInt32(s);
                        languageDetail.LanguageId = id;
                        languageDetail.PersonId = person.PersonId;
                        db.LanguageDetails.Add(languageDetail);
                        db.SaveChanges();
                        s = "";
                        continue;
                    }
                    s += i;
                }
                return RedirectToAction("Index");
            }
            else
            {
                foreach (var l in lang)
                {
                    personInfo.Languages.Add(new Language { LanguageId = l.LanguageId, LanguageName = l.LanguageName, IsSeleted = false });
                }
                ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", personInfo.CityId);
                ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "CountryName", personInfo.CountryId);
                return View(personInfo);
            }
            
        }

        public ActionResult Edit(int? id)
        {
            PersonInfo personInfo = db.PersonInfos.Find(id);

            ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", personInfo.CityId);
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "CountryName", personInfo.CountryId);

            List<Language> lang = new List<Language>();
            List<Language> list = new List<Language>();
           
            lang = db.Languages.ToList();
            int idd = 0;
            foreach (var l in lang)
            {
                idd = l.LanguageId;
                var nn = personInfo.LanguageDetails.Select(x => x.LanguageId).ToList();//.Find(l.LanguageId);
               
                if(nn.Contains(idd))
                {
                    list.Add(new Language { LanguageId = l.LanguageId, LanguageName = l.LanguageName, IsSeleted = true });
                }
                else
                {
                    list.Add(new Language { LanguageId = l.LanguageId, LanguageName = l.LanguageName, IsSeleted = false });
                }
             
            }
            ViewBag.LanguageDetail = list;
            return View(personInfo);
        }
        
        [HttpPost]
        public ActionResult Edit(PersonInfo personInfo, string hobbies, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                string fileName = null;
                if (file != null)
                {
                    fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    string extension = Path.GetExtension(file.FileName);
                    fileName = fileName + "_" + DateTime.Now.ToString("yymmssfff") + extension;
                    string filePath = Path.Combine(Server.MapPath("~/Images"), fileName);
                    file.SaveAs(filePath);
                    personInfo.Resume = fileName;
                }

                db.Entry(personInfo).State = EntityState.Modified;
                db.SaveChanges();

                LanguageDetail languageDetail = new LanguageDetail();
                var langDetail = db.LanguageDetails.Where(x => x.PersonId == personInfo.PersonId).ToList();
                foreach (var d in langDetail)
                {
                    db.LanguageDetails.Remove(d);
                    db.SaveChanges();
                }

                hobbies = hobbies + ",";
                string s = "";
                foreach (var i in hobbies)
                {
                    if (i == ',')
                    {
                        int id = Convert.ToInt32(s);
                        languageDetail.LanguageId = id;
                        languageDetail.PersonId = personInfo.PersonId;
                        db.LanguageDetails.Add(languageDetail);
                        db.SaveChanges();
                        s = "";
                        continue;
                    }
                    s += i;
                }
                return RedirectToAction("Index");
            }
            else
            {
                PersonInfo person = db.PersonInfos.Find(personInfo.PersonId);
                List<Language> lang = new List<Language>();
                List<Language> list = new List<Language>();

                lang = db.Languages.ToList();
                int idd = 0;
                foreach (var l in lang)
                {
                    idd = l.LanguageId;
                    var nn = person.LanguageDetails.Select(x => x.LanguageId).ToList();//.Find(l.LanguageId);

                    if (nn.Contains(idd))
                    {
                        list.Add(new Language { LanguageId = l.LanguageId, LanguageName = l.LanguageName, IsSeleted = true });
                    }
                    else
                    {
                        list.Add(new Language { LanguageId = l.LanguageId, LanguageName = l.LanguageName, IsSeleted = false });
                    }

                }
                ViewBag.LanguageDetail = list;
                ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", person.CityId);
                ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "CountryName", person.CountryId);
                return View(person);
            }
            
        }
        
        public ActionResult Delete(int? id)
        {
            PersonInfo personInfo = db.PersonInfos.Find(id);
            return View(personInfo);
        }
        
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PersonInfo personInfo = db.PersonInfos.Find(id);
            db.PersonInfos.Remove(personInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
    }
}
