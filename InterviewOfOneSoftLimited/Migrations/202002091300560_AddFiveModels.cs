﻿namespace InterviewOfOneSoftLimited.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFiveModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        CityId = c.Int(nullable: false, identity: true),
                        CityName = c.String(),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CityId)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        CountryId = c.Int(nullable: false, identity: true),
                        CountryName = c.String(),
                    })
                .PrimaryKey(t => t.CountryId);
            
            CreateTable(
                "dbo.PersonInfoes",
                c => new
                    {
                        PersonId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 32),
                        CountryId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false),
                        Resume = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.PersonId)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: false)
                .Index(t => t.CountryId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.LanguageDetails",
                c => new
                    {
                        LanguageDetailId = c.Int(nullable: false, identity: true),
                        LanguageId = c.Int(nullable: false),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LanguageDetailId)
                .ForeignKey("dbo.Languages", t => t.LanguageId, cascadeDelete: true)
                .ForeignKey("dbo.PersonInfoes", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.LanguageId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        LanguageId = c.Int(nullable: false, identity: true),
                        LanguageName = c.String(),
                    })
                .PrimaryKey(t => t.LanguageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cities", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.LanguageDetails", "PersonId", "dbo.PersonInfoes");
            DropForeignKey("dbo.LanguageDetails", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.PersonInfoes", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.PersonInfoes", "CityId", "dbo.Cities");
            DropIndex("dbo.LanguageDetails", new[] { "PersonId" });
            DropIndex("dbo.LanguageDetails", new[] { "LanguageId" });
            DropIndex("dbo.PersonInfoes", new[] { "CityId" });
            DropIndex("dbo.PersonInfoes", new[] { "CountryId" });
            DropIndex("dbo.Cities", new[] { "CountryId" });
            DropTable("dbo.Languages");
            DropTable("dbo.LanguageDetails");
            DropTable("dbo.PersonInfoes");
            DropTable("dbo.Countries");
            DropTable("dbo.Cities");
        }
    }
}
