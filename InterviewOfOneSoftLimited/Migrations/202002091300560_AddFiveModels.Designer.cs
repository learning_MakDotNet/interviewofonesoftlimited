﻿// <auto-generated />
namespace InterviewOfOneSoftLimited.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class AddFiveModels : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddFiveModels));
        
        string IMigrationMetadata.Id
        {
            get { return "202002091300560_AddFiveModels"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
